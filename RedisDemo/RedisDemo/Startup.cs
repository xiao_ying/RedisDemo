using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Redis;
using ShopDAL.Models;
using Microsoft.EntityFrameworkCore;

namespace RedisDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //注册上下文对象
            services.AddDbContext<ShopContext>(options => {
                options.UseSqlServer(Configuration.GetConnectionString("NetCoreDB"));
            });

            RedisCacheOptions options = new RedisCacheOptions();
            options.Configuration = Configuration.GetSection("RedisDB:Configuration").Value;
            options.InstanceName = Configuration.GetSection("RedisDB:InstanceName").Value;
            var redisClient = new RedisCache(options);

            services.AddSingleton(redisClient);//全局注入

            //使用Session服务
            services.AddSession();//启用session

            //将session数据存储到Redis中
            services.AddSingleton<IDistributedCache>(serviceProvider=>redisClient);

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseSession();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
