﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RedisDemo.Models;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Redis;
using Microsoft.AspNetCore.Http;

namespace RedisDemo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            //使用session用redis作为存储媒介
            HttpContext.Session.SetString("mySession-string","Jack");
            HttpContext.Session.SetInt32("mySession-int", 666);



            /*//使用redis
            RedisCacheOptions options = new RedisCacheOptions();
            options.Configuration = "127.0.0.1:6379";
            options.InstanceName = "myRedis";
            //创建数据
            var redisClient = new RedisCache(options);
            redisClient.SetString("name", $"张三！{DateTime.Now}");
            //设置绝对过期时间
            redisClient.SetString("name1", $"张三！{DateTime.Now}", new DistributedCacheEntryOptions().SetAbsoluteExpiration(DateTime.Now.AddMinutes(2)));
            //设置相对过期时间
            redisClient.SetString("name2", $"张三！{DateTime.Now}", new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(2)));
            ViewBag.Msg = "数据缓存成功";*/
            return View();
        }

        public IActionResult Privacy()
        {
            //读取session

            var result1 = HttpContext.Session.GetString("mySession-string");
            var result2 = HttpContext.Session.GetInt32("mySession-int");

            /* //读取数据
             RedisCacheOptions options = new RedisCacheOptions();
             options.Configuration = "127.0.0.1:6379";
             options.InstanceName = "myRedis";
             //创建数据
             var redisClient = new RedisCache(options);
             String result = redisClient.GetString("name");
             ViewBag.Msg = "数据读取成功"; 
             ViewBag.result = result;


             String result1 = redisClient.GetString("name1");
             String result2 = redisClient.GetString("name2");*/

            ViewBag.result1 = result1;
            ViewBag.result2 = result2;
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
