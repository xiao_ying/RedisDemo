﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Redis;
using ShopDAL.Models;
using Newtonsoft.Json;

namespace RedisDemo.Controllers
{
    public class ProductController : Controller
    {
        private ShopContext ShopDB;
        private RedisCache redis;
        public ProductController(ShopContext shopContext,RedisCache redisCache)
        {
            ShopDB = shopContext;
            redis = redisCache;
        }
        public IActionResult Index()
        {
             
            string prodJson = redis.GetString("prods");
            if (string.IsNullOrEmpty(prodJson)) {
                var productList = ShopDB.Products.ToList();
                prodJson = JsonConvert.SerializeObject(productList);
                redis.SetString("prods", prodJson);
                return View(productList);
            }
            else
            {
                IEnumerable<Products> productList = JsonConvert.DeserializeObject<IEnumerable<Products>>(prodJson);
                return View(productList);
            }
        }
    }
}